﻿using JoomlaPackager.Classes;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows.Forms;
using System.Configuration;
using System.Diagnostics;
using System.Linq;

namespace JoomlaPackager
{
    public partial class frmMain : Form
    {
        public frmMain() => InitializeComponent();

	    private List<DatasourceVersions> SortList(List<DatasourceVersions> versies)
	    {
			var a = new List<int>();
		    foreach (var item in versies)
		    {
			    int.TryParse(item.Name.Replace(".", string.Empty), out var b);
				if (b > 0)
					a.Add(b);
		    }

		    var i = 1;
			var sorted = SortArray(a.ToArray()).ToList();
		    foreach (var id in sorted)
		    {
			    var b = versies.First(c => c.Sortkey == id);
			    b.Order = i;
			    i++;
		    }

		    return versies.OrderByDescending(b => b.Order).ToList();
	    }
	    public static int[] SortArray(int[] array)
	    {
		    var length = array.Length;

		    for (var i = 0; i < length; i++)
		    {
			    for (var j = i + 1; j < length; j++)
			    {
				    if (array[i] > array[j])
				    {
					    var temp = array[i];

					    array[i] = array[j];

					    array[j] = temp;
				    }
			    }
		    }

		    return array;
	    }

		private void frmMain_Load(object sender, EventArgs e)
        {
            #region Joomla versies ophalen en plaatsen in een combobox
            var fileman = new FileManagement();
            fileman.MaakMap("tijdelijk");
            using (var client = new WebClient())
            {
                client.DownloadFile(ConfigurationManager.AppSettings["JoomlaVersionInfoUrl"], @"tijdelijk/versieDoc.xml");
            }
            var jVersies = fileman.GetXmlValues(@"tijdelijk/versieDoc.xml", ConfigurationManager.AppSettings["JoomlaVersionInfoTag"], ConfigurationManager.AppSettings["JoomlaVersionInfoAttribute"]);
            var versies = new List<DatasourceVersions>();
            var i = 1;
            foreach (var jVersie in jVersies)
            {
                var ver = jVersie.RemoveLast(".");
                if (!versies.Exists(a => a.Name == ver))
                {
	                int.TryParse(ver.Replace(".", string.Empty), out var nver);
	                if (nver > 0)
	                {
		                var versie = new DatasourceVersions {Id = i, Name = ver, Sortkey = nver, Order = 0};
		                versies.Add(versie);
		                i++;
	                }
                }
            }
			versies = SortList(versies);
            cbJoomlaVersion.DataSource = versies;
            cbJoomlaVersion.DisplayMember = "Name";
            cbJoomlaVersion.ValueMember = "Id";
            #endregion Joomla versies ophalen en plaatsen in een combobox
            if (versies[0]?.Name != null)
                Program.LogMessage($"Hoogst gevonden versie {versies[0].Name}.");

            fileman.WisBestandenInMap("tijdelijk");
            fileman.VerwijderMap("tijdelijk");
	        lblVersion.Text = $@"v{FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).FileVersion}";
		}

        private void lblLegal_Click(object sender, EventArgs e) => System.Diagnostics.Process.Start("http://www.joomlanl.nl/");

	    private void btnGeneratePackage_Click(object sender, EventArgs e)
        {
            var fileman = new FileManagement();

            try
            {
                btnGeneratePackage.Enabled = false;

                fileman.MaakMap("tijdelijk");
                fileman.WisBestandenInMap("tijdelijk");
                
                var location = AppDomain.CurrentDomain.BaseDirectory;
                var version = cbJoomlaVersion.Text;
                var github = cbGithub.Checked;
                var joomlaVersionSeperator = (github ? ConfigurationManager.AppSettings["GithubDownloadSeperator"] : ConfigurationManager.AppSettings["JoomlaOrgDownloadSeperator"]);
                var dowloadVersion = version.Replace(".", joomlaVersionSeperator);
                var joomlaDownloadUrl = (github ? ConfigurationManager.AppSettings["GithubDownloadUrl"].Replace("VERSIENUMMER", dowloadVersion) : ConfigurationManager.AppSettings["JoomlaOrgDownloadUrl"].Replace("VERSIENUMMER", dowloadVersion));

                
                var bewerkLijst = new List<VervangWaardes>();
                fileman.VerwijderBestand(@"tijdelijk/Joomla.zip");
                fileman.VerwijderBestand(@"tijdelijk/Taalbestanden.zip");

                Program.LogMessage("Dit process kan even duren... Log zal niet leesbaar zijn tot het process klaar is.");

                //Downloaden
                using (var client = new WebClient())
                {
                    Program.LogMessage($"Downloaden: {joomlaDownloadUrl}");
                    client.DownloadFile(joomlaDownloadUrl, @"tijdelijk/Joomla.zip");
                    Program.LogMessage($@"Opgeslagen als: {location}tijdelijk\Joomla.zip");
                    Program.LogMessage($"Downloaden: {ConfigurationManager.AppSettings["LanguageFilesDownloadUrl"]}");
                    client.DownloadFile(ConfigurationManager.AppSettings["LanguageFilesDownloadUrl"], @"tijdelijk/Taalbestanden.zip");
                    Program.LogMessage($@"Opgeslagen als: {location}tijdelijk\Taalbestanden.zip");
                    Program.LogMessage($"Downloaden: {ConfigurationManager.AppSettings["SqlDataDownloadUrl"]}");
                    client.DownloadFile(ConfigurationManager.AppSettings["SqlDataDownloadUrl"], @"tijdelijk/Sqldata.zip");
                    Program.LogMessage($@"Opgeslagen als: {location}tijdelijk\Sqldata.zip");
                }

                //Uitpakken
                Program.LogMessage($@"Unzippen: {location}tijdelijk\Joomla.zip");
                fileman.UnzipBestanden(@"tijdelijk\Joomla.zip", @"tijdelijk\unzip\Joomla");
                Program.LogMessage($@"Unzippen: {location}tijdelijk\Taalbestanden.zip");
                fileman.UnzipBestanden(@"tijdelijk\Taalbestanden.zip", @"tijdelijk\unzip\Taalbestanden");
                Program.LogMessage($@"Unzippen: {location}tijdelijk\Sqldata.zip");
                fileman.UnzipBestanden(@"tijdelijk\Sqldata.zip", @"tijdelijk\unzip\Sqldata");

                //Taaldata
                Program.LogMessage("Toevoegen van Nederlandse taalstring voor voorbeelddata.");
                fileman.VoegTaalstringsVoorbeeldataToe(@"tijdelijk\unzip\Joomla\installation\language\nl-NL\nl-NL.ini");

                //Kopieren van Joomla
                Program.LogMessage("Joomla kopieren voor naar het nieuwe installatie pakket");
                fileman.KopieerAlles(@"tijdelijk\unzip\Joomla", @"tijdelijk\package");

                // Kopieren taalafhankelijke dingen
                Program.LogMessage("Nederlandse taalbestanden kopieren.");
                fileman.KopieerAlles(@"tijdelijk\unzip\Taalbestanden\staging", @"tijdelijk\package");
                fileman.KopieerBestand(@"tijdelijk\unzip\Taalbestanden\staging\pkg_nl-NL.xml", @"tijdelijk\package\administrator\manifests\packages\pkg_nl-NL.xml");

                // Speciaal gedoe voor Nederlands installatiepakket
                Program.LogMessage("Aantal zaken uitvoeren, mbt Nederlandse SQL data.");
                fileman.VerwijderBestand(@"tijdelijk\package\installation\localise.xml");
                bewerkLijst.Add(new VervangWaardes() { Vervanging = version, Zoek = "VERSIE" });
                fileman.BewerkBestand(@"tijdelijk\unzip\Sqldata\JoomlaSqlData-master\localise.xml", bewerkLijst, @"tijdelijk\unzip\Sqldata\JoomlaSqlData-master\localise.xml_");
                fileman.KopieerBestand(@"tijdelijk\unzip\Sqldata\JoomlaSqlData-master\localise.xml_", @"tijdelijk\package\installation\localise.xml");
                bewerkLijst.Clear();
                bewerkLijst.AddRange(new List<VervangWaardes>() {
                    new VervangWaardes() {Vervanging = version, Zoek = "VERSIE"},
                    new VervangWaardes() {Vervanging = DateTime.Now.ToString("MMMM yyyy"), Zoek = "DATUM"}
                });
                fileman.BewerkBestand(@"tijdelijk\unzip\Sqldata\JoomlaSqlData-master\sql\mysql\localise.sql", bewerkLijst, @"tijdelijk\unzip\Sqldata\JoomlaSqlData-master\sql\mysql\localise.sql_");
                fileman.KopieerBestand(@"tijdelijk\unzip\Sqldata\JoomlaSqlData-master\sql\mysql\localise.sql_", @"tijdelijk\package\installation\sql\mysql\localise.sql");
                fileman.KopieerBestand(@"tijdelijk\unzip\Sqldata\JoomlaSqlData-master\sql\mysql\sample_data_nl.sql", @"tijdelijk\package\installation\sql\mysql\sample_data_nl.sql");

                // Zippen
                fileman.VerwijderBestand($@"Joomla_{version}-Stable-Full_Package_Dutch.zip");
                Program.LogMessage("Het geheel zippen.");
                fileman.ZipBestanden(@"tijdelijk\package", $@"Joomla_{version}-Stable-Full_Package_Dutch.zip");
                Program.LogMessage($@"Klaar! {location}Joomla_{version}-Stable-Full_Package_Dutch.zip");

            }
            catch(Exception ex)
            {
                Program.LogMessage("Er is iets fout gegaan met het maken van het installatie pakket.");
            }
            finally
            {
                fileman.WisBestandenInMap("tijdelijk");
                fileman.VerwijderMap("tijdelijk");
                btnGeneratePackage.Enabled = true;
            }
        }

        #region Logging
        /// <summary>
        /// Wissen van alles wat tot nu toe in het log staat.
        /// </summary>
        private void btnClearLog_Click(object sender, EventArgs e) => lstLog.Items.Clear();

	    public void SetLogText(String text) => lstLog.Items.Insert(0, text);

	    #endregion Logging

	}
}
