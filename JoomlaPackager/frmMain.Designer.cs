﻿namespace JoomlaPackager
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
			this.lblLegal = new System.Windows.Forms.Label();
			this.lblVersion = new System.Windows.Forms.Label();
			this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
			this.cbJoomlaVersion = new System.Windows.Forms.ComboBox();
			this.lblJoomlaVersion = new System.Windows.Forms.Label();
			this.btnGeneratePackage = new System.Windows.Forms.Button();
			this.lstLog = new System.Windows.Forms.ListBox();
			this.btnClearLog = new System.Windows.Forms.Button();
			this.cbGithub = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// lblLegal
			// 
			this.lblLegal.AutoSize = true;
			this.lblLegal.Location = new System.Drawing.Point(16, 225);
			this.lblLegal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblLegal.Name = "lblLegal";
			this.lblLegal.Size = new System.Drawing.Size(208, 17);
			this.lblLegal.TabIndex = 1;
			this.lblLegal.Text = "Gemaakt door Team Joomla!NL";
			this.lblLegal.Click += new System.EventHandler(this.lblLegal_Click);
			// 
			// lblVersion
			// 
			this.lblVersion.AutoSize = true;
			this.lblVersion.Location = new System.Drawing.Point(865, 225);
			this.lblVersion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblVersion.Name = "lblVersion";
			this.lblVersion.Size = new System.Drawing.Size(0, 17);
			this.lblVersion.TabIndex = 2;
			// 
			// cbJoomlaVersion
			// 
			this.cbJoomlaVersion.FormattingEnabled = true;
			this.cbJoomlaVersion.Location = new System.Drawing.Point(123, 14);
			this.cbJoomlaVersion.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.cbJoomlaVersion.Name = "cbJoomlaVersion";
			this.cbJoomlaVersion.Size = new System.Drawing.Size(140, 24);
			this.cbJoomlaVersion.TabIndex = 4;
			// 
			// lblJoomlaVersion
			// 
			this.lblJoomlaVersion.AutoSize = true;
			this.lblJoomlaVersion.Location = new System.Drawing.Point(16, 17);
			this.lblJoomlaVersion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblJoomlaVersion.Name = "lblJoomlaVersion";
			this.lblJoomlaVersion.Size = new System.Drawing.Size(99, 17);
			this.lblJoomlaVersion.TabIndex = 5;
			this.lblJoomlaVersion.Text = "Joomla versie:";
			// 
			// btnGeneratePackage
			// 
			this.btnGeneratePackage.Location = new System.Drawing.Point(320, 11);
			this.btnGeneratePackage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.btnGeneratePackage.Name = "btnGeneratePackage";
			this.btnGeneratePackage.Size = new System.Drawing.Size(155, 28);
			this.btnGeneratePackage.TabIndex = 6;
			this.btnGeneratePackage.Text = "&Maak pakketje";
			this.btnGeneratePackage.UseVisualStyleBackColor = true;
			this.btnGeneratePackage.Click += new System.EventHandler(this.btnGeneratePackage_Click);
			// 
			// lstLog
			// 
			this.lstLog.FormattingEnabled = true;
			this.lstLog.ItemHeight = 16;
			this.lstLog.Location = new System.Drawing.Point(20, 50);
			this.lstLog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.lstLog.Name = "lstLog";
			this.lstLog.Size = new System.Drawing.Size(909, 164);
			this.lstLog.TabIndex = 7;
			// 
			// btnClearLog
			// 
			this.btnClearLog.Location = new System.Drawing.Point(831, 11);
			this.btnClearLog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.btnClearLog.Name = "btnClearLog";
			this.btnClearLog.Size = new System.Drawing.Size(100, 28);
			this.btnClearLog.TabIndex = 8;
			this.btnClearLog.Text = "&Wis log";
			this.btnClearLog.UseVisualStyleBackColor = true;
			this.btnClearLog.Click += new System.EventHandler(this.btnClearLog_Click);
			// 
			// cbGithub
			// 
			this.cbGithub.AutoSize = true;
			this.cbGithub.Checked = true;
			this.cbGithub.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbGithub.Location = new System.Drawing.Point(528, 16);
			this.cbGithub.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.cbGithub.Name = "cbGithub";
			this.cbGithub.Size = new System.Drawing.Size(241, 21);
			this.cbGithub.TabIndex = 9;
			this.cbGithub.Text = "&Download Joomla via Github URL";
			this.cbGithub.UseVisualStyleBackColor = true;
			// 
			// frmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(952, 252);
			this.Controls.Add(this.cbGithub);
			this.Controls.Add(this.btnClearLog);
			this.Controls.Add(this.lstLog);
			this.Controls.Add(this.btnGeneratePackage);
			this.Controls.Add(this.lblJoomlaVersion);
			this.Controls.Add(this.cbJoomlaVersion);
			this.Controls.Add(this.lblVersion);
			this.Controls.Add(this.lblLegal);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.MaximizeBox = false;
			this.Name = "frmMain";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Joomla packager";
			this.Load += new System.EventHandler(this.frmMain_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblLegal;
        private System.Windows.Forms.Label lblVersion;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ComboBox cbJoomlaVersion;
        private System.Windows.Forms.Label lblJoomlaVersion;
        private System.Windows.Forms.Button btnGeneratePackage;
        private System.Windows.Forms.ListBox lstLog;
        private System.Windows.Forms.Button btnClearLog;
        private System.Windows.Forms.CheckBox cbGithub;
    }
}

