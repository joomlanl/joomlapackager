﻿using System;

namespace JoomlaPackager.Classes
{
    public static class ExtensionMethods
    {
        public static string RemoveLast(this string text, string character)
        {
            int index = text.LastIndexOf(character, StringComparison.Ordinal);
            if (index > 0)
                text = text.Substring(0, index);
            return text;
        }
    }
}
