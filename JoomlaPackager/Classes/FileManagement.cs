﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Xml;

namespace JoomlaPackager.Classes
{
    /// <summary>
    /// Class met de methods die nodig zijn voor alle zaken mbt bestanden en mappen
    /// </summary>
    //TODO: Foutafhandeling verbeteren
    public class FileManagement
    {
        /// <summary>
        /// Voegt de taalstrings voor de voorbeelddata toe aan het opgegeven bestand
        /// </summary>
        /// <param name="pad">Pad naar bestand</param>
        //TODO: Gegevens die toegevoegd worden afsplitsen, en nieuwe method maken die de regels append bij een bestand
        public void VoegTaalstringsVoorbeeldataToe(string pad)
        {
            using (var w = File.AppendText(pad))
            {
                w.WriteLine("INSTL_SAMPLE_DATA_NL_SET=\"Standaard Nederlandse voorbeelddata\"");
                w.WriteLine("INSTL_SAMPLE_DATA_NL_SET_DESC=\"Installeer Joomla met een pagina(een menu met een link) en modules zoals Nieuwste artikel, Inlogformulier.(Nederlands)\"");
            }
        }

        /// <summary>
        /// Wissen van alles in de gekozen map
        /// </summary>
        /// <param name="folder">Map</param>
        public void WisBestandenInMap(string folder)
        {
            Console.WriteLine($"Bezig met het wissen van de inhoud van {folder}.");
            try
            {
                var di = new DirectoryInfo(folder);

                foreach (var file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach (var dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }
                Console.WriteLine("Succesvol verwijderd.");
            }
            catch (Exception)
            {
                Console.WriteLine("Fout bij het verwijderen.");
            }
        }

        /// <summary>
        /// Uitpakken van een zip bestand
        /// </summary>
        /// <param name="zip">Pad van het zipbestand</param>
        /// <param name="unzipPad">Pad voor de uitvoer</param>
        public void UnzipBestanden(string zip, string unzipPad)
        {
            Console.WriteLine($"Uitpakken van {zip} naar {unzipPad}");
            try
            {
                ZipFile.ExtractToDirectory(zip, unzipPad);
                Console.WriteLine($"{zip} is succesvol uitgepakt.");
            }
            catch (Exception)
            {
                Console.WriteLine($"Fout bij het uitpakken van {zip}");
            }

        }

        /// <summary>
        /// Inpakken van de bestanden in een map naar een zip bestand
        /// </summary>
        /// <param name="bron">Map met data die ingepakt moet worden</param>
        /// <param name="doel">Bestandsnaam en locatie van het te maken zipbestand</param>
        public void ZipBestanden(string bron, string doel)
        {
            Console.WriteLine($"Inpakken van {bron} naar {doel}");
            try
            {
                ZipFile.CreateFromDirectory(bron, doel);
                Console.WriteLine($"{doel} is aangemaakt.");
            }
            catch (Exception)
            {
                Console.WriteLine($"Fout bij het aanmaken van {doel}");
            }
        }

        /// <summary>
        /// Kopieeren van alle bestanden en mappen van een locatie naar een andere
        /// </summary>
        /// <param name="bronPad">Bronpad</param>
        /// <param name="doelPad">Doelpad</param>
        public void KopieerAlles(string bronPad, string doelPad)
        {
            Console.WriteLine($"Kopieren van {bronPad} naar {doelPad}");
            MaakMap(doelPad);
            foreach (var dirPath in Directory.GetDirectories(bronPad, "*", SearchOption.AllDirectories))
                Directory.CreateDirectory(dirPath.Replace(bronPad, doelPad));
            foreach (var newPath in Directory.GetFiles(bronPad, "*", SearchOption.AllDirectories))
                File.Copy(newPath, newPath.Replace(bronPad, doelPad), true);
        }

        /// <summary>
        /// Maken van een nieuwemap
        /// </summary>
        /// <param name="pad">Het pad met de naam van de nieuwe map</param>
        public void MaakMap(string pad)
        {
            if (!Directory.Exists(pad))
                Directory.CreateDirectory(pad);
        }

        /// <summary>
        /// Kopieeren van een enkel bestand, eventueel bestaand bestand wordt overschreven
        /// </summary>
        /// <param name="bron">Bronpad</param>
        /// <param name="doel">Doelpad</param>
        public void KopieerBestand(string bron, string doel)
        {
            var fi = new FileInfo(bron);
            fi.CopyTo(doel, true);
        }

        /// <summary>
        /// Bewerken van een bestand
        /// </summary>
        /// <param name="fileName">Invoer bestand</param>
        /// <param name="vervangWaardes">Een lijst van het type <see cref="VervangWaardes"/></param>
        /// <param name="saveFileName">Uitvoer bestand</param>
        public void BewerkBestand(string fileName, List<VervangWaardes> vervangWaardes, string saveFileName)
        {
            var reader = new StreamReader(fileName);
            var input = reader.ReadToEnd();

            using (var writer = new StreamWriter(saveFileName, true))
            {
                foreach (var vervangen in vervangWaardes)
                {
                    input = input.Replace(vervangen.Zoek, vervangen.Vervanging);
                }
                writer.Write(input);
                writer.Close();
            }
        }

        /// <summary>
        /// Verwijderen van een enkel bestand
        /// </summary>
        /// <param name="pad">Pad van het bestand</param>
        public void VerwijderBestand(string pad)
        {
            try
            {
                if (File.Exists(pad))
                    File.Delete(pad);
            }
            catch (Exception)
            {
                //foutje bij het verwijderen, in gebruik?
            }
        }

        /// <summary>
        /// Verwijderen van een enkele map
        /// </summary>
        /// <param name="pad">Pad van de map</param>
        public void VerwijderMap(string pad)
        {
            try
            {
                if (Directory.Exists(pad))
                    Directory.Delete(pad);
            }
            catch (Exception)
            {
                //foutje bij verwijderen map, in gebruik?
            }
        }

        /// <summary>
        /// Ophalen van gegevens in een xml bestand waarvan de node matched
        /// </summary>
        /// <param name="pad">Pad van het xml bestand</param>
        /// <param name="tagName">Naam van de node</param>
        /// <returns>Geeft een list met strings terug</returns>
        public List<string> GetXmlValues(string pad, string tagName, string attribute)
        {
            var xmlFile = File.ReadAllText(pad);
            var xmldoc = new XmlDocument();
            xmldoc.LoadXml(xmlFile);
            var nodeList = xmldoc.GetElementsByTagName(tagName);
            var xmldata = (from XmlNode node in nodeList from XmlNode val in node.Attributes where val.Name == attribute select val.Value).ToList();
            var result = (from a in xmldata
                          orderby a descending
                          select a).Distinct().ToList();
            return result;
        }

        public XmlNodeList GetXmlValueFromUrl(string pad, string tagName)
        {
            var doc = new XmlDocument();
            doc.Load(pad);
            var root = doc.DocumentElement;
            return root?.SelectNodes(tagName);
        }
    }

    /// <summary>
    /// Struct, gegevens die vervangen moeten worden hebben de properties
    /// </summary>
    public struct VervangWaardes
    {
        public string Zoek { get; set; }
        public string Vervanging { get; set; }
    }
}
