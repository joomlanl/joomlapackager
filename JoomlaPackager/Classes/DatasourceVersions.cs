﻿namespace JoomlaPackager.Classes
{
    public class DatasourceVersions
    {
        public int Id { get; set; }
        public string Name { get; set; }

	    public int Sortkey { get; set; }

	    public int Order { get; set; }
    }
}
