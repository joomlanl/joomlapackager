﻿using JoomlaPackager.Classes;
using System;
using System.Windows.Forms;

namespace JoomlaPackager
{
    public static class Program
    {
        private static frmMain MainForm { get; set; }
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            AppDomain.CurrentDomain.ProcessExit += OnProcessExit;
            ClearTemp();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            MainForm = new frmMain();
            Application.Run(MainForm);
        }

        public static void LogMessage(string message)
        {
            MainForm.SetLogText(message);
        }

        private static void OnProcessExit(object sender, EventArgs e)
        {
            ClearTemp();
        }

        public static void ClearTemp()
        {
            var fileman = new FileManagement();
            fileman.WisBestandenInMap("tijdelijk");
            fileman.VerwijderMap("tijdelijk");
        }
    }
}
